const PeerId = require('peer-id');
const P2P = require('libp2p');
const { NOISE } = require('libp2p-noise');
const MPLEX = require('libp2p-mplex');
const MDNS = require('libp2p-mdns');
const Websockets = require('libp2p-websockets');
const TCP = require('libp2p-tcp');

module.exports = async ({ port, peer }) => ({
  peerId: await PeerId.createFromJSON(peer),
  addresses: {
    listen: [
      `/ip4/0.0.0.0/tcp/${port + 1}`,
      `/ip4/0.0.0.0/tcp/${port}/ws`,
    ],
  },
  modules: {
    transport: [TCP, Websockets],
    connEncryption: [NOISE],
    streamMuxer: [MPLEX],
    peerDiscovery: [MDNS],
  },
  config: {
    relay: {
      enabled: true,
      hop: { enabled: true, active: true },
      advertise: { enabled: false },
    },
  },
});
