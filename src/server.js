const P2P = require('libp2p');
const PeerId = require('peer-id');
const pipe = require('it-pipe');
const { decode } = require('it-length-prefixed');
const p2pConfig = require('./libp2p-server');
const identities = require('../identities.json');

if (process.argv.length <= 2) {
  throw new Error('Pass a server identity name');
}

const identity = identities[process.argv[2]];

if (!identity) {
  throw new Error(`No identity named "${process.argv[2]}"`);
}

async function main() {
  const p2p = await P2P.create(await p2pConfig(identity));

  p2p.connectionManager.on('peer:connect', (conn) => {
    console.log('Connected:', conn.remotePeer.toB58String());
  });

  p2p.connectionManager.on('peer:disconnect', (conn) => {
    console.log('Disconnected:', conn.remotePeer.toB58String());
  });

  p2p.on('peer:discovery', (peer) => {
    console.log('Discovered:', peer.toB58String());
  });

  p2p.handle('/test', (conn) => {
    pipe(conn.stream, decode(), async (source) => {
      for await (const chunk of source) {
        console.log('>', Buffer.from(chunk.slice()).toString());
      }
    });
  });

  await p2p.start();

  p2p.multiaddrs.forEach((addr) => {
    console.log(`- ${addr}/p2p/${p2p.peerId.toB58String()}`);
  });
}

main();
