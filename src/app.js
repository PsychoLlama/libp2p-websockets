import P2P from 'libp2p';
import pipe from 'it-pipe';
import { encode, decode } from 'it-length-prefixed';
import p2pConfig from './libp2p-browser';
import serverMadr from './server-madr';

async function main() {
  const p2p = await P2P.create(p2pConfig);

  p2p.handle('/test', (conn) => {
    pipe(conn.stream, decode(), async (source) => {
      for await (const chunk of source) {
        console.log('>', Buffer.from(chunk.slice()).toString());
      }
    });
  });

  await p2p.start();
  console.log('ID:', p2p.peerId.toB58String());

  const conn = await p2p.dialProtocol(serverMadr, '/test');
  pipe(['hello, world'], encode(), conn.stream);
}

main();
