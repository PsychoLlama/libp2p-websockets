import identities from '../identities.json';

const params = new URLSearchParams(location.search);
const addr = params.has('host') ? params.get('host') : '127.0.0.1';

if (!params.has('server')) {
  throw new Error('Choose a server identity (the "server" query parameter).')
}

const identity = identities[params.get('server')];

if (!identity) {
  throw new Error(`No identity named "${params.get('server')}"`);
}

export default `/ip4/${addr}/tcp/${identity.port}/ws/p2p/${identity.peer.id}`;
