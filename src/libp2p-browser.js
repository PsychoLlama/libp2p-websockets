import P2P from 'libp2p';
import { NOISE } from 'libp2p-noise';
import MPLEX from 'libp2p-mplex';
import Websockets from 'libp2p-websockets';
import Bootstrap from 'libp2p-bootstrap';
import filters from 'libp2p-websockets/src/filters';
import serverMadr from './server-madr';

const Tag = {
  Websockets: Websockets.prototype[Symbol.toStringTag],
  Bootstrap: Bootstrap.tag,
};

module.exports = {
  addresses: {
    listen: [
      `${serverMadr}/p2p-circuit`,
    ],
  },
  modules: {
    transport: [Websockets],
    connEncryption: [NOISE],
    streamMuxer: [MPLEX],
    peerDiscovery: [Bootstrap],
  },
  config: {
    peerDiscovery: {
      [Bootstrap.tag]: {
        list: [serverMadr],
      },
    },
    transport: {
      [Tag.Websockets]: {
        filter: filters.all,
      },
    },
    relay: {
      enabled: true,
    },
    nat: {
      enabled: false,
    },
  },
};
