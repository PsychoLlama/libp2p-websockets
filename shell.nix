config@{ pkgs ? import <nixpkgs> config }:

pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    python3
    gnumake
    binutils
  ];
}
