# Websocket Browser Relay
Browser to browser communication through websocket relays.

## Usage
Connect at least two browser tabs, then copy the server address and the browser address. Interpolate both peer IDs into a string like this:

```
/ip4/127.0.0.1/tcp/8082/ws/p2p/<relay-peer-id>/p2p-circuit/p2p/<browser-peer-id>
```

Use that multiaddr to dial the other browser from the console.
